const mongoose = require('mongoose')
const cartSchema = new mongoose.Schema({

    user : { type : mongoose.Schema.Types.ObjectId, ref: 'User', required : true},
    isActive: {type: Boolean, default: true},
    cartItems : [
        {
            product: {
                type: String ,
                required : true 
            },
            name: {
                type: String ,
                 required : true 
             },
            quantity: {
                type: Number ,
                 default : 1 
             },
            price: {
                type: Number ,
                 required: true
             },
            subtotal: {
                type: Number ,
                 required: true
             }
        }
    ]

})

module.exports = mongoose.model('Cart', cartSchema)